import csv
import os

csv_namafile = 'kontak.csv'

def bersihkan_layar():
    os.system('cls' if os.name == 'nt' else 'clear')
    # moudul os dengan fungsi sistem untuk membersihkan layar.

def tampilkan_menu():
    bersihkan_layar()
    print("=== APLIKASI KONTAK ===")
    print("[1] Lihat Daftar Kontak")
    print("[2] Buat Kontak")
    print("[3] Ubah Kontak")
    print("[4] Hapus Kontak")
    print("[5] Cari Kontak")
    print("\n")
    print("[0] Keluar")
    print("\n")
    print("__" * 10)
    print("\n")
    
    pilihan_menu = input("Pilih Menu> ")

    if(pilihan_menu == "1"):
        tampilkan_kontak()
    elif(pilihan_menu == "2"):
        buat_kontak()
    elif(pilihan_menu == "3"):
        ubah_kontak()
    elif(pilihan_menu == "4"):
        hapus_kontak()
    elif(pilihan_menu == "5"):
        cari_kontak()
    elif(pilihan_menu == "0"):
        exit()
    else:
        print("Kamu Memilih Menu Yang Salah!\n",
                "Pilih Menu 1-5 atau 0 Untuk Keluar")
        kembali_ke_menu()

def kembali_ke_menu():
    print("\n")
    input("Tekan Enter Untuk Kembali: ")
    tampilkan_menu()

# Membuat Fungsi Tampilkan Kontak - No.1
def tampilkan_kontak():
    bersihkan_layar()
    kontak = []
    with open(csv_namafile) as csvfile:
        csv_reader = csv.reader(csvfile, delimiter=",")
        for row in csv_reader:
            kontak.append(row)

    if (len(kontak) > 0):
        labels = kontak.pop(0)
        print(f"{labels[0]} \t {labels[1]} \t\t {labels[2]}")
        print("__" * 20)
        for data in kontak:
            print(f'{data[0]} \t {data[1]} \t {data[2]}')
    else:
        print("Tidak Ada Data!")

    kembali_ke_menu()

# Membuat Fungsi Buah Kontak - No.2
def buat_kontak():
    bersihkan_layar()
    with open(csv_namafile, mode='a') as csv_file:
        fieldnames = ['No', 'Nama', 'Telepon']
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)

        no = input("No Urut: ")
        nama = input("Nama Lengkap: ")
        telepon = input("No. Telepon: ")

        writer.writerow({'No': no, 'Nama': nama, 'Telepon': telepon})
        print("Berhasil Disimpan!")

    kembali_ke_menu()

# Membuat Fungsi Ubah Kontak - No.3
def ubah_kontak():
    bersihkan_layar()
    kontak = []

    with open(csv_namafile, mode='r') as csv_file:
        csv_reader = csv.DictReader(csv_file)
        for row in csv_reader:
            kontak.append(row)

    print("No \t Nama \t\t Telepon")
    print("__" * 20)

    for data in kontak:
        print(f"{data['No']} \t {data['Nama']} \t {data['Telepon']}")

    print("__" * 20)

    no = input("Pilih Nomor Kontak> ")
    nama = input("Nama Baru: ")
    telepon = input("Nomor Telepon Baru: ")

    # Mencari Kontak & Mengubah Data Dengan Data Yang Baru
    indeks = 0
    for data in kontak:
        if (data['No'] == no):
            kontak[indeks]['Nama'] = nama
            kontak[indeks]['Telepon'] = telepon
        indeks = indeks + 1

    # Menulis Data Baru Ke File CSV (Tulis Ulang)
    with open(csv_namafile, mode='w') as csv_file:
        fieldnames = ['No', 'Nama', 'Telepon']
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        writer.writeheader()
        for new_data in kontak:
            writer.writerow({'No': new_data['No'], 'Nama': new_data['Nama'], 'Telepon': new_data['Telepon']})

    kembali_ke_menu()

# Membuat Fungsi Hapus Kontak - No.4
def hapus_kontak():
    bersihkan_layar()
    kontak = []

    with open(csv_namafile, mode='r') as csv_file:
        csv_reader = csv.DictReader(csv_file)
        for row in csv_reader:
            kontak.append(row)

    print("No \t Nama \t\t Telepon")
    print("__" * 20)

    for data in kontak:
        print(f"{data['No']} \t {data['Nama']} \t {data['Telepon']}")

    print("__" * 20)

    no = input("Hapus Nomor> ")

    # Mencari Kontak Dan Mengubah Data Dengan Data Yang Baru
    indeks = 0
    for data in kontak:
        if (data['No'] == no):
            kontak.remove(kontak[indeks])
        indeks = indeks + 1

    # Menulis Data Baru Ke File CSV (Tulis Ulang)
    with open(csv_namafile, mode='w') as csv_file:
        fieldnames = ['No', 'Nama', 'Telepon']
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        writer.writeheader()
        for new_data in kontak:
            writer.writerow({'No': new_data['No'], 'Nama': new_data['Nama'], 'Telepon': new_data['Telepon']})

    print("Data Berhasil Dihapus")

    kembali_ke_menu()

# Membuat Fungsi Cari Kontak - No.5
def cari_kontak():
    bersihkan_layar()
    kontak = []

    with open(csv_namafile, mode='r') as csv_file:
        csv_reader = csv.DictReader(csv_file)
        for row in csv_reader:
            kontak.append(row)

    no = input("Cari Berdasarkan Nomor Urut> ")

    data_ditemukan = []

    # Mencari Kontak
    indeks = 0
    for data in kontak:
        if (data['No'] == no):
            data_ditemukan = kontak[indeks]

        indeks = indeks + 1

    if len(data_ditemukan) > 0:
        print("Data Ditemukan: ")
        print(f"Nama: {data_ditemukan['Nama']}")
        print(f"Telepon: {data_ditemukan['Telepon']}")
    else:
        print("Tidak Ada Data Ditemukan")

    kembali_ke_menu()

# Main 
if __name__ == "__main__":
    while True:
        tampilkan_menu()
